import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'utils/notifications/Notifications.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Birds",
          style: TextStyle(fontSize: 30.0),
        ),
        backgroundColor: Colors.blue[700],
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.menu),
              onPressed: () {
                Scaffold.of(context).openDrawer();
              },
              tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
            );
          },
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.add_alert),
            tooltip: 'Will Show Notification',
            onPressed: () {
              notificationAlert(context);
            },
          ),
        ],
      ),
      drawer: Drawer(
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: Text("Lun Sim"),
              accountEmail: Text("lunsim168@gmail.com"),
              decoration: BoxDecoration(
                color: const Color(0xff7c94b6),
                image: const DecorationImage(
                  image: NetworkImage('https://lh3.googleusercontent.com/proxy/8bj5CUIjOnb-5h_ZYcwMkzEME9adz1kFqJI7EoeUR3OW9bD4xATfMDwJquj0nE8oU1R2ApQrDDSbS9zsGIY'),
                  fit: BoxFit.cover,
                ),
                border: Border.all(
                  color: Colors.black,
                  width: 1,
                ),
                borderRadius: BorderRadius.circular(1),
              ),
              currentAccountPicture: CircleAvatar(
                backgroundColor: Colors.blue[900],
                child: Image.network("https://lh3.googleusercontent.com/proxy/8bj5CUIjOnb-5h_ZYcwMkzEME9adz1kFqJI7EoeUR3OW9bD4xATfMDwJquj0nE8oU1R2ApQrDDSbS9zsGIY"),
              ),
            ),
            ListTile(
              leading: Icon(Icons.monetization_on),
              title: Text("Coupong"),
              onTap: () {
                Navigator.of(context).pushNamed("/coupong");
              },
            ),
            ListTile(
              leading: Icon(Icons.shopping_cart),
              title: Text("Shopping"),
              onTap: () {
                Navigator.of(context).pushNamed("/shopping");
              },
            ),
            ListTile(
              leading: Icon(Icons.receipt),
              title: Text("Receipt"),
              onTap: () {
                Navigator.of(context).pushNamed("/receipt");
              },
            ),
            ListTile(
              leading: Icon(Icons.record_voice_over),
              title: Text("Receipt Report"),
              onTap: () {
                Navigator.of(context).pushNamed("/report");
              },
            ),
          ],
        ),
      ),
      body: myBody(context),
    );
  }
}


myBody(BuildContext context) {
  return Container(
    child: Scaffold(
      body: Container(
        child: Column(
          children: [
            new Container(
              height: 230.0,
              alignment: Alignment.center,
              color: Colors.blueAccent,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  new Container(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text(
                        "រូបិយប័ណ្ណ",
                        style: TextStyle(fontSize: 15.0, color: Colors.white),
                      ),
                    ),
                  ),
                  new Container(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text(
                        "9876543.85 USD",
                        style: TextStyle(fontSize: 25.0, color: Colors.white),
                      ),
                    ),
                  ),
                  new Container(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 5.0),
                      child: Text(
                        "លេខគណនី",
                        style: TextStyle(fontSize: 15.0, color: Colors.white),
                      ),
                    ),
                  ),
                  new Container(
                      child: Padding(
                    padding: const EdgeInsets.only(top: 5.0),
                    child: FlatButton(
                      child: Text(
                        "របៀបផ្ទេរប្រាក់",
                        style: TextStyle(color: Colors.white, fontSize: 15.0),
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(20.0),
                        side: BorderSide(color: Colors.white),
                      ),
                      onPressed: () {},
                    ),
                  )),
                ],
              ),
            ),
            new Container(
              child: GridView.count(
                shrinkWrap: true,
                crossAxisCount: 2,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10),
                            bottomLeft: Radius.circular(10),
                            bottomRight: Radius.circular(10)),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 5,
                            blurRadius: 8,
                            offset: Offset(0, 3), // changes position of shadow
                          ),
                        ],
                      ),
                      child: InkWell(
                        onTap: () {
                          Navigator.of(context).pushNamed("/coupong");
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Container(
                              padding: EdgeInsets.all(10.0),
                              child: Column(
                                children: [
                                  Icon(
                                    Icons.monetization_on,
                                    size: 80.0,
                                    color: Colors.lightBlue,
                                  ),
                                  Container(
                                    child: Text(
                                      "Coupong",
                                      style: TextStyle(
                                          fontSize: 20.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.blue),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10),
                            bottomLeft: Radius.circular(10),
                            bottomRight: Radius.circular(10)),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: Offset(0, 3), // changes position of shadow
                          ),
                        ],
                      ),
                      child: InkWell(
                        onTap: () {
                          Navigator.of(context).pushNamed("/shopping");
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Container(
                                child: Container(
                              padding: EdgeInsets.all(10.0),
                              child: Column(
                                children: [
                                  Icon(
                                    Icons.shopping_cart,
                                    size: 80.0,
                                    color: Colors.lightBlue,
                                  ),
                                  Container(
                                    child: Text(
                                      "Shopping",
                                      style: TextStyle(
                                          fontSize: 20.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.blue),
                                    ),
                                  ),
                                ],
                              ),
                            )),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10),
                            bottomLeft: Radius.circular(10),
                            bottomRight: Radius.circular(10)),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: Offset(0, 3), // changes position of shadow
                          ),
                        ],
                      ),
                      child: InkWell(
                        onTap: () {
                          Navigator.of(context).pushNamed("/receipt");
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Container(
                              padding: EdgeInsets.all(10.0),
                              child: Column(
                                children: [
                                  Icon(
                                    Icons.receipt,
                                    size: 80.0,
                                    color: Colors.lightBlue,
                                  ),
                                  Container(
                                    child: Text(
                                      "Receipt",
                                      style: TextStyle(
                                          fontSize: 20.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.blue),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10),
                            bottomLeft: Radius.circular(10),
                            bottomRight: Radius.circular(10)),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: Offset(0, 3), // changes position of shadow
                          ),
                        ],
                      ),
                      child: InkWell(
                        onTap: () {
                          Navigator.of(context).pushNamed("/report");
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Container(
                                child: Container(
                              padding: EdgeInsets.all(10.0),
                              child: Column(
                                children: [
                                  Icon(
                                    Icons.record_voice_over,
                                    size: 80.0,
                                    color: Colors.lightBlue,
                                  ),
                                  Container(
                                    child: Text(
                                      "Receipt Report",
                                      style: TextStyle(
                                          fontSize: 20.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.blue),
                                    ),
                                  ),
                                ],
                              ),
                            )),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    ),
  );
}
