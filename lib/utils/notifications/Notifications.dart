import 'package:flutter/material.dart';

void notificationAlert(BuildContext context) {
  // flutter defined function
  showDialog(
    context: context,
    builder: (BuildContext context) {
      // return object of type Dialog
      return AlertDialog(
        title: new Text("お知らせ"),
        content: new Text("おひさしぶり!\nおあいできてうれしいです。\n どもう、ありがとうございます!"),
        actions: <Widget>[
          // usually buttons at the bottom of the dialog
          new FlatButton(
            child: new Text("閉じる"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}