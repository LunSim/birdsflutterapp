import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:learn/ShoppingDetails.dart';
import 'CoupongDetails.dart';
import 'Homepage.dart';
import 'ReceiptDetails.dart';
import 'ReceiptReportDetails.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      //home: HomePage(),
      debugShowCheckedModeBanner: false,
      initialRoute: "/",
      routes: {
        "/": (context) => HomePage(),
        "/coupong": (context) => CoupongDetails(),
        "/shopping": (context) => ShoppingDetails(),
        "/receipt": (context) => ReceiptDetails(),
        "/report": (context) => ReceiptReportDetails(),
      },
    );
  }
}
